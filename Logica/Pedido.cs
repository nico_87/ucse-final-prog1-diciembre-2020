﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class Pedido
    {
        public int NumeroPedido { get; set; }
        public int DniCliente { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaEntrega { get; set; }
        public double CostoVenta { get; set; }
        public Pago Forma { get; set; }
        public DateTime FechaPago { get; set; }


        public enum Pago { Efectivo, Transferencia, Mercadopago }
    }
}
