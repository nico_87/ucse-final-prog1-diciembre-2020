﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class Principal
    {
        public List<Cliente> Clientes { get; set; }
        public List<Pedido> Pedidos { get; set; }
        public List<Juego> Juegos { get; set; }

        //Corrección: La respuesta de este método debía ser una clase que tenga dos propiedades un booleano y un string, sino el mensaje no lo devolves nunca.
        public bool AltaCliente(ref string mensaje, int dni, string nombre, string domicilio, string telefono)
        {
            Cliente clienteEncontrado = null;
            clienteEncontrado = Clientes.FirstOrDefault(x=>x.Documento == dni);
            if (clienteEncontrado!=null)
            {
                mensaje = " El cliente esta registrado ";
                return false;

            }
            if (nombre!=null && domicilio!= null && telefono != null)
            {
                Cliente nuevoCliente = new Cliente();
                nuevoCliente.NombreApellido = nombre;
                nuevoCliente.Documento = dni;
                nuevoCliente.Domicilio = domicilio;
                nuevoCliente.Telefono = telefono;
                Clientes.Add(nuevoCliente);

                mensaje = "Se registro  el nuevo cliente";
                return true;
            }
            mensaje = "No se puede registrar nuevo cliente por falta de datos importantes";
            return false;
        }

        public List<string> ListadoTableros()
        {
            List<string> listadoGenerado = new List<string>();
            foreach (var item in Juegos)
            {
                string elemento = item.Detalle();
                listadoGenerado.Add(elemento);
            }
            return listadoGenerado;
        }

        public void AltaPedido(int dniCliente, int nroTablero)
        {
            Juego juegoEncontrado = null;
            juegoEncontrado = Juegos.FirstOrDefault(x=>x.Codigo == nroTablero);
            if (juegoEncontrado != null)
            {
                Pedido pedidoNuevo = new Pedido();
                pedidoNuevo.DniCliente = dniCliente;
                pedidoNuevo.FechaPedido = DateTime.Today;
                pedidoNuevo.NumeroPedido = Pedidos.Count + 1;
                int diasEntrega = juegoEncontrado.CalcularEntrega();
                pedidoNuevo.FechaEntrega = DateTime.Today.AddDays(diasEntrega);
                pedidoNuevo.CostoVenta = juegoEncontrado.PrecioBase + 80 * diasEntrega;

                Pedidos.Add(pedidoNuevo);               
            }
        }

        public void ActualizarPedido(int idPedido, string formaPago)
        {
            Pedido pedidoEncontrado = null;
            pedidoEncontrado = Pedidos.FirstOrDefault(x=>x.NumeroPedido == idPedido);
            if (pedidoEncontrado!=null)
            {
                pedidoEncontrado.FechaPago = DateTime.Today;
                //Corrección: Aca el parámetro formaÂgo deberia ser del tipo Pedido.Pago
                pedidoEncontrado.Forma = formaPago;
            }
        }



    }
}
