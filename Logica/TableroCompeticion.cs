﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class TableroCompeticion : Juego
    {
        public string Color { get; set; }
        public Categoria Categorias { get; set; }
        public int CantidadJugadores { get; set; }





        public enum Categoria { Deporte, Didáctico, Estrategia }



        public override string Detalle()
        {
            return "Tipo: Competicion " + base.Detalle() + " Cantidad Jugadores :" + CantidadJugadores + " Categoria" + Categorias;
        }

        public override int CalcularEntrega()
        {
            string dato = Categorias.ToString();
            //Aca en lugar de comparar como string se debería comparar con el enumerador: Categorias == Categoria.Estrategia
            if (dato == "Estrategia")
            {
                return 10;
            }
            return 15;
            
        }
    }
}
