﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    abstract class Juego
    {
        public int Codigo { get; set; }
        public int Largo { get; set; }
        public int Ancho { get; set; }
        //Corrección: La edad era una edad desde y una edad hasta.
        public int EdadRecomendacion { get; set; }
        public double PrecioBase { get; set; }

        public virtual string Detalle()
        {
            return "Codigo: " + Codigo + "Tamaño: " + Largo + "x" + Ancho;
        }

        public abstract int CalcularEntrega();
    }
}
