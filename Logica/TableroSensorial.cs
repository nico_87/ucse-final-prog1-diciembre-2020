﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class TableroSensorial : Juego
    {
        public int CantidadJuegos { get; set; }
        public string Color { get; set; }
        public bool Personalizado { get; set; }

        public override int CalcularEntrega()
        { 
            if (Personalizado == false)
            {
                return 10;
            }
            return 14;
        }

        public override string Detalle()
        {
            return "Tipo : Sensorial "+ base.Detalle()+ "Cantidad de Juegos: "+CantidadJuegos+" Perdonalizado: "+ Personalizado;
        }


    }
}
