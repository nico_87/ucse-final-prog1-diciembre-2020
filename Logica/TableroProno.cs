﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class TableroProno : Juego
    {
        public int CantidadBarras { get; set; }
        public bool Personalizado { get; set; }
        public List<string> Colores { get; set; }

        public override int CalcularEntrega()
        {
            int agregar = 0;
            if (Colores.Count == 3)
            {
                agregar += 3;
            }
            if (CantidadBarras <= 3)
            {
                agregar += 10;
                return agregar;
            }
            agregar += 15;
            return agregar;           
        }

        public override string Detalle()
        {
            return "Tipo: Prono "+ base.Detalle()+ "Cantidad barras: "+ CantidadBarras+ "Personalizado: "+Personalizado;
        }

    }
}
