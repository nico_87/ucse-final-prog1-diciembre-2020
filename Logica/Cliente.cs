﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class Cliente
    {
        public int Documento { get; set; }
        public string NombreApellido { get; set; }
        public string CodigoPostal { get; set; }
        public string Domicilio { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }

    }
}
